package com.example.sqlsimple;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHandler extends SQLiteOpenHelper {

	
	public DBHandler(Context context) 
	{
		super(context, "contactsDBName", null, 1);
	}

	//This method is called when the database is first time created on the device.
	@Override
	public void onCreate(SQLiteDatabase db) {
		// SQL command: create table 
		String CREATE_CONTACTS_TABLE = "CREATE TABLE tableContact (id INTEGER PRIMARY KEY, name  TEXT, phone_number TEXT  )";
		db.execSQL(CREATE_CONTACTS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
				// This is not how its done in real time
				// In real world, new table is created, contents from old table copied and then old table is dropped.
				// For this method to be called version number should be different.
				
		         //db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
		         db.execSQL("DROP TABLE IF EXISTS tableContact" );
				// Create tables again
				onCreate(db);

	}

}
