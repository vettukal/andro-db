package com.example.sqlsimple;

import java.util.ArrayList;
import java.util.List;


import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		DBHandler dbhelper = new DBHandler(this);
		SQLiteDatabase db = dbhelper.getReadableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("name","Ravi" ); // Contact Name
		values.put("phone_number","998989"); // Contact Phone
		db.insert("contactsTable", null, values);
		
		 values = new ContentValues();
		values.put("name", "Suresh"); // Contact Name
		values.put("phone_number","74125"); // Contact Phone
		db.insert("contactsTable", null, values);
		
		 values = new ContentValues();
		values.put("name", "Kalmadi"); // Contact Name
		values.put("phone_number","4209211"); // Contact Phone
		db.insert("contactsTable", null, values);
		
		 values = new ContentValues();
		values.put("name","A. Raja" ); // Contact Name
		values.put("phone_number","666777"); // Contact Phone
		db.insert("contactsTable", null, values);
		
		//db.execSQL("INSERT INTO "+"contactsTable"+"  VALUES(1012,\"VINCENT\",\"9035241250\")");
		
		// Select All Query
		String selectQuery = "SELECT  * FROM contactsTable" ;

		
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				
				Log.d("venture",cursor.getString(0));
				Log.d("venture",cursor.getString(1));
				Log.d("venture",cursor.getString(2));
				
			} while (cursor.moveToNext());
		}
		
		//Example end

		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
