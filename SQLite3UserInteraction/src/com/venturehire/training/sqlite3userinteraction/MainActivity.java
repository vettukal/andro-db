package com.venturehire.training.sqlite3userinteraction;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity implements OnClickListener {

	Button mInsert, mShow;
	EditText mName, mLocation, mPhone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mInsert = (Button) findViewById(R.id.insert);
		mShow = (Button) findViewById(R.id.show);

		mName = (EditText) findViewById(R.id.nameEdt);
		mLocation = (EditText) findViewById(R.id.locationEdt);
		mPhone = (EditText) findViewById(R.id.phoneEdt);

		mInsert.setOnClickListener(this);
		mShow.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v == mInsert) {

			String nameStr = mName.getText().toString();
			String locationStr = mLocation.getText().toString();
			String phoneStr = mPhone.getText().toString();

			DBHandler db = new DBHandler(this);

			db.addContact(new Contact(nameStr, locationStr, phoneStr));

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Values are inserted in the DB!")
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();

									mName.setText("");
									mLocation.setText("");
									mPhone.setText("");
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		}
		if (v == mShow) {

			Intent intent = new Intent(this, ReadDBActivity.class);
			startActivity(intent);

		}
	}

}
