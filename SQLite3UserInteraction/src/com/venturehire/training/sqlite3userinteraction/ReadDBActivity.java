package com.venturehire.training.sqlite3userinteraction;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ReadDBActivity extends Activity {

	TextView mName, mLocation, mPhone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.readdb_activity);

		mName = (TextView) findViewById(R.id.nameTxt);
		mLocation = (TextView) findViewById(R.id.locationTxt);
		mPhone = (TextView) findViewById(R.id.phoneTxt);

		DBHandler db = new DBHandler(this);

		List<Contact> contacts = db.getAllContacts();

		for (Contact cn : contacts) {
			String nameStr = cn.getName();
			String locationStr = cn.getLocation();
			String phoneStr = cn.getPhoneNumber();

			mName.setText("Name: " + nameStr);
			mLocation.setText("Location: " + locationStr);
			mPhone.setText("Phone Number: " + phoneStr);

		}
	}

}
