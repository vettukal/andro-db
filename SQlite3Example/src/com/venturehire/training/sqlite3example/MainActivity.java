package com.venturehire.training.sqlite3example;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {
	private static final String TABLE_CONTACTS = "contactsTable";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		DBHandler db = new DBHandler(this);

		/**
		 * CRUD Operations
		 * */ 
		// Inserting Contacts
		Log.d("Insert: ", "Inserting ..");
		db.addContact(new Contact("Ravi", "9100000000"));
		db.addContact(new Contact("Srinivas", "9199999999"));
		db.addContact(new Contact("Tommy", "9522222222"));
		db.addContact(new Contact("Karthik", "9533333333"));
		db.getWritableDatabase().execSQL("INSERT INTO "+TABLE_CONTACTS+"  VALUES(1012,\"VINCENT\",\"9035241250\")");

		// Reading all contacts
		Log.d("Reading: ", "Reading all contacts..");
		List<Contact> contacts = db.getAllContacts();

		for (Contact cn : contacts) {
			String log = "Id: " + cn.getID() + " ,Name: " + cn.getName()
					+ " ,Phone: " + cn.getPhoneNumber();
			// Writing Contacts to log
			Log.d("Name: ", log);
		}
	}

}
